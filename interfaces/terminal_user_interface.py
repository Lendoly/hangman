import sys

from os import system

from constants import MAX_FAILURES, GAME_OVER_MESSAGE, TEXT_START_OPTIONS, TEXT_NEW_GAME
from controllers.controller import HangmanController


class HangmanTerminalInterface:
    player_name = None

    def __init__(self):
        self.controller = HangmanController()

    def _clear(self):
        """
        Clean the terminal
        :IMPORTANT: This only clean the terminalon Linux and MacOX probably will make it fail on Windows
        """
        _ = system("clear")

    def _print_errors(self):
        """
        Get the message form teh controller and print it on the terminal
        """
        message = self.controller.get_error_message()
        if message:
            print(message)

    def _print_info(self):
        """
        Print tot he terminal the basic info about the game ongoing
        """
        print(f"Game id: {self.controller.game_id}")
        print(f"Number of Errors: {self.controller.num_errors}")
        print(
            f"You still have {MAX_FAILURES - self.controller.num_errors} errors before game over"
        )
        print(f"Score: {self.controller.score}\n")

    def _print_game_over(self):
        print(GAME_OVER_MESSAGE)
        print(f"Final score: {self.controller.score}")

    def _print_wining(self):
        print("CONGRATULATIONS!!")
        print(f"Final score: {self.controller.score}")

    def _print_new_game_message(self):
        print(TEXT_NEW_GAME)

    def _new_hangman_game(self):
        """
        manage the interface fpr the new game
        :return:
        """

        self.controller.new_game(player_name=self.player_name)
        self._print_new_game_message()

        # loop that will keep the game going until is finished
        while not self.controller.end:
            self._print_info()
            self._print_errors()

            print(self.controller.get_word_with_underscores())
            letter = input("next letter: ")

            self.controller.handle_input(letter)
            self._clear()

        if self.controller.win:
            self._print_wining()
        else:
            self._print_game_over()

        # wait for the user confirmation to keep the execution
        _ = input("press to exit")
        self.controller.save_game()
        self._clear()

    def _print_leader_board(self):
        print("RANK\tNAME\tSCORE")
        for num, element in enumerate(self.controller.get_ranking()):
            print(f"{num+1}\t{element}")
        _ = input("press to exit")
        self._clear()

    def start(self):
        """
        Start the game by terminal, asking first for the name and for the first possible options
        """
        OPTIONS = {0: sys.exit, 1: self._new_hangman_game, 2: self._print_leader_board}
        input_error = False
        self.player_name = input("Please tell me your name: ")

        while True:
            self._clear()
            try:
                if input_error:
                    print("wrong option, choose one between 0 and 2")
                    input_error = False
                welcome_option = int(
                    input(TEXT_START_OPTIONS.format(user_name=self.player_name))
                )
                OPTIONS.get(welcome_option)()
            except ValueError:
                input_error = True

            except TypeError:
                input_error = True
