from flask import Blueprint, request, jsonify

from controllers.controller import HangmanController

bp = Blueprint("hangman", __name__, url_prefix="/hangman")


@bp.route("/game", methods=["POST"])
def new_game():
    if not request.json or "player_name" not in request.json:
        return jsonify(status=f"field player_name is mandatory"), 400

    player_name = request.json.get("player_name")

    controller = HangmanController()
    controller.new_game(player_name)
    controller.save_game()
    return jsonify(game_info=controller.serializer()), 201


@bp.route("/game/<string:game_id>", methods=["GET"])
def get_game_info(game_id):
    controller = HangmanController()
    controller.load_game(game_id)
    if controller.hangman:
        return jsonify(game_info=controller.serializer()), 200
    else:
        return jsonify(status=f"Game {game_id} Not found"), 404


@bp.route("/game/<string:game_id>/letter", methods=["POST"])
def retrieve_letter(game_id):
    controller = HangmanController()
    controller.load_game(game_id)

    if not controller.hangman:
        return jsonify(status=f"Game {game_id} Not found"), 404

    # if the game is finished is not possible to add more letter
    if controller.end:
        return (
            jsonify(
                status=f"Game {game_id} is already finished",
                game_info=controller.serializer(),
            ),
            403,
        )

    if not request.json or "letter" not in request.json:
        return jsonify(status=f"field letter is mandatory"), 400

    letter = request.json.get("letter")

    controller.handle_input(letter)
    controller.save_game(new=False)
    message_error = controller.get_error_message()
    if message_error:
        # If the game finish with this letter instead of sending the message of error
        # we send the message of game over
        if controller.end:
            return (
                jsonify(
                    status="BEST LUCK NEXT TIME! THE GAME I OVER!",
                    game_info=controller.serializer(),
                ),
                202,
            )
        return jsonify(status=message_error, game_info=controller.serializer()), 202

    # If the game ends and the user won we send the message of congratulations
    if controller.end and controller.win:
        jsonify(status="CONGRATULATIONS!!", game_info=controller.serializer()), 202

    return jsonify(game_info=controller.serializer()), 202


@bp.route("/leaderboard", methods=["GET"])
def get_leaderboards():
    controller = HangmanController()
    leaderboard = []
    for num, element in enumerate(controller.get_ranking()):
        leaderboard.append({"position": num + 1, "game_info": element.serializer()})

    return jsonify(data=leaderboard, total_results=len(leaderboard)), 200
