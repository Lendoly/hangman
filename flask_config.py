import os


class BaseConfig:
    """Base configuration"""

    TESTING = False
    DEBUG = True
    SECRET_KEY = "my_precious"


class LocalConfig(BaseConfig):
    """Development configuration"""

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")


class TestingConfig(BaseConfig):
    """Test environment configuration"""

    TESTING = True
