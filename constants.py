MAX_FAILURES = 5
TOP_RANKING = 100
POSSIBLE_WORDS = ["3dhubs", "marvin", "print", "filament", "order", "layer"]
DATABASE_HOST = "sqlite:///games.db"

GAME_OVER_MESSAGE = """
      ::::::::     :::      :::   :::  ::::::::::
    :+:    :+:  :+: :+:   :+:+: :+:+: :+:
   +:+        +:+   +:+ +:+ +:+:+ +:++:+
  :#:       +#++:++#++:+#+  +:+  +#++#++:++#
 +#+   +#+#+#+     +#++#+       +#++#+
#+#    #+##+#     #+##+#       #+##+#
######## ###     ######       #############
      :::::::: :::     ::::::::::::::::::::::
    :+:    :+::+:     :+::+:       :+:    :+:
   +:+    +:++:+     +:++:+       +:+    +:+
  +#+    +:++#+     +:++#++:++#  +#++:++#:
 +#+    +#+ +#+   +#+ +#+       +#+    +#+
#+#    #+#  #+#+#+#  #+#       #+#    #+#
########     ###    #############    ###
"""


TEXT_START_OPTIONS = """
Welcome {user_name} to the Hangman game what do you want to do:
    0 - Exit
    1 - Start a new game
    2 - Check the leaderboard
==> Option to run (eg: 1)
==> """

TEXT_NEW_GAME = f"""
Starting Hangman game, In case you never had childhood and played it between lessons here is brief summary:

    - This game is about to guess one word that the machine will propose to you.
    - At the start yu will only see underscores like this one '_', one underscore per letter
    - The game will ask for one letter, if the letter that you introduce is valid the underscores will reveal letter

So for example '_ _ _ _ _' for the word 'o r d e r' if you introduce the letter r it will change to:
'_ r _ _ r'

At every step the game will ask you for a letter

IMPORTANT RULES:
    - If you introduce a total of {MAX_FAILURES} letters that are not in the word you will fail.
    - Every letter correct is 10 points (so if it appear twice is 20 points)
    - Every letter failed is -5 points
Good luck!
"""
