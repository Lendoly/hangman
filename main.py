import argparse

from interfaces.terminal_user_interface import HangmanTerminalInterface
from flask_app import create_app


def get_args():
    parser = argparse.ArgumentParser(
        description="Main to launch flask server or console game",
        epilog="example: python main.py terminal",
    )

    parser.add_argument(
        "interface",
        action="store",
        help="Which interface to execute options are: terminal and flask",
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    if args.interface.lower() == "terminal":
        game = HangmanTerminalInterface()
        game.start()
    elif args.interface.lower() == "flask":
        app = create_app()
        app.run(host="0.0.0.0")
    else:
        print("interface invalid the only possible values are: terminal and flask")
