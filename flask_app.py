import os

from flask import Flask, jsonify


def create_app():
    # create and configure the app
    app = Flask(__name__)
    app.config.from_object("flask_config")

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    @app.route("/ping")
    def hello():
        return jsonify(message="pong")

    from interfaces import api_user_interface

    app.register_blueprint(api_user_interface.bp)

    return app
