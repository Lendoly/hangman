import json
from collections import Counter
from copy import copy

from constants import POSSIBLE_WORDS
from models.hangman import Hangman, CORRECT_POINTS, INCORRECT_POINTS

DEFAULT_DICT = {
    "id": 1,
    "game_id": "a23",
    "word": "test",
    "guessed_letters": '{"t":2}',
    "wrong_guessed_letters": "a,b,c",
    "num_errors": 3,
    "status": 1,
    "score": 10,
    "player_name": "luis",
}


def test_constructor_without_player_name_default():
    hangman = Hangman()

    assert not hangman.id
    assert hangman.game_id
    assert hangman.word in POSSIBLE_WORDS
    assert hangman.guessed_letters == Counter()
    assert hangman.num_errors == 0
    assert hangman.wrong_guessed_letters == []
    assert hangman.status == 0
    assert hangman.score == 0
    assert not hangman.player_name


def test_constructor_with_player_name_defaut():
    hangman = Hangman(player_name="Luis")

    assert not hangman.id
    assert hangman.game_id
    assert hangman.word in POSSIBLE_WORDS
    assert hangman.guessed_letters == Counter()
    assert hangman.wrong_guessed_letters == []
    assert hangman.status == 0
    assert hangman.score == 0
    assert hangman.player_name == "Luis"
    assert hangman.num_errors == 0


def test_constructor_with_dict_on_kwargs():
    hangman = Hangman(**DEFAULT_DICT)

    assert hangman.id == DEFAULT_DICT.get("id")
    assert hangman.game_id == DEFAULT_DICT.get("game_id")
    assert hangman.word == DEFAULT_DICT.get("word")
    assert hangman.guessed_letters == Counter(
        json.loads(DEFAULT_DICT.get("guessed_letters"))
    )
    assert hangman.wrong_guessed_letters == DEFAULT_DICT.get(
        "wrong_guessed_letters"
    ).split(",")
    assert hangman.status == DEFAULT_DICT.get("status")
    assert hangman.score == DEFAULT_DICT.get("score")
    assert hangman.player_name == DEFAULT_DICT.get("player_name")
    assert hangman.num_errors == DEFAULT_DICT.get("num_errors")


def test_letter_already_used_in_wrong_true():
    hangman = Hangman(**DEFAULT_DICT)

    wrong_letters = DEFAULT_DICT.get("wrong_guessed_letters").split(",")
    assert hangman.letter_already_used(wrong_letters[0])


def test_letter_already_used_correct_true():
    hangman = Hangman(**DEFAULT_DICT)

    correct_letters = Counter(json.loads(DEFAULT_DICT.get("guessed_letters")))
    most_comon_tuple = correct_letters.most_common()[0]
    assert hangman.letter_already_used(most_comon_tuple[0])


def test_letter_already_used_false():
    hangman = Hangman(**DEFAULT_DICT)
    assert not hangman.letter_already_used("w")


def test_letter_on_hide_word_true():
    hangman = Hangman(**DEFAULT_DICT)
    letter = "e"
    assert hangman.letter_on_hide_word(letter)
    assert letter in hangman.guessed_letters


def test_letter_on_hide_word_false():
    hangman = Hangman(**DEFAULT_DICT)

    letter = "W"
    assert not hangman.letter_on_hide_word(letter)
    assert letter in hangman.wrong_guessed_letters


def test_increase_score_zero_letters():
    hangman = Hangman(**DEFAULT_DICT)
    letter = "p"

    hangman.increase_score(letter)
    assert hangman.score == DEFAULT_DICT.get("score")


def test_increase_score_one_letters():
    hangman = Hangman(**DEFAULT_DICT)
    letter = "e"

    hangman.increase_score(letter)
    assert hangman.score == DEFAULT_DICT.get("score") + CORRECT_POINTS


def test_increase_score_two_letters():
    hangman = Hangman(**DEFAULT_DICT)
    letter = "t"

    hangman.increase_score(letter)
    assert hangman.score == DEFAULT_DICT.get("score") + CORRECT_POINTS * 2


def test_add_error():
    hangman = Hangman(**DEFAULT_DICT)

    hangman.add_error()
    assert hangman.num_errors == DEFAULT_DICT.get("num_errors") + 1
    assert hangman.score == DEFAULT_DICT.get("score") - INCORRECT_POINTS


def test_correct_property_true():
    hangman = Hangman(**DEFAULT_DICT)
    hangman.letter_on_hide_word("e")
    hangman.letter_on_hide_word("s")

    assert hangman.correct


def test_correct_property_false():
    hangman = Hangman(**DEFAULT_DICT)

    assert not hangman.correct


def test_serializer():
    hangman = Hangman(**DEFAULT_DICT)
    new_dict = copy(DEFAULT_DICT)
    new_dict["guessed_letters"] = {"t": 2}
    new_dict["wrong_guessed_letters"] = ["a", "b", "c"]
    new_dict.pop("id")
    new_dict.pop("word")

    assert hangman.serializer() == new_dict
