from constants import MAX_FAILURES, TOP_RANKING
from controllers.exceptions import ExceptionGameNotInitialized

from models.hangman import Hangman


class HangmanController:
    """
    Controller for the Game hangman
    """

    error_type = None
    hangman = None

    ERROR_TYPES = {
        1: "UPS the letter is already used!",
        2: "UPS the letter is not on the word!",
        3: "UPS you can only introduce only one letter at the time!",
        4: "UPS that blank is not gonna help you to win",
    }

    def new_game(self, player_name):
        """
        Start a new game and creates a new instance for a give player name
        :param player_name: name for the player
        :return: None
        """
        self.hangman = Hangman(player_name=player_name)

    def get_word_with_underscores(self):
        """
        Will convert the word to guess to underscores if the letters has not being guessed yet
        """
        word_with_underscores = ""
        for letter in self.hangman.word:
            if letter in self.hangman.guessed_letters:
                word_with_underscores += letter
            else:
                word_with_underscores += "_"
        return " ".join(letter for letter in word_with_underscores)

    def _handle_error(self, error_type):
        """
        Method that handle the business logic if the letter is not into the word.
        :param error_type: type of error that hapend
        :return: None
        """
        self.error_type = error_type
        self.hangman.add_error()
        if self.num_errors >= MAX_FAILURES:
            self.hangman.status = 1

    def _handle_success(self, letter):
        """
        Method that handle te business logic for a correct guess
        :param letter: letter that was guessed correct
        :return: None
        """
        self.error_type = None
        self.hangman.increase_score(letter)
        if self.hangman.correct:
            self.hangman.status = 1

    def get_error_message(self):
        """
        If a error happened will return a message for that error
        :return: string with more info about the error
        :rtype: str
        """
        if self.error_type:
            return self.ERROR_TYPES.get(self.error_type)

    def handle_input(self, letter):
        """
        Given a letter it check it and makes the correct actions in every step;
        - check that there is actually a letter
        - checks that the letter is not in fact the keyword exit which will finish any ongoing game
        - check that there is only 1 letter and not more
        - check if the letter was already used
        - check if the word is or not into the word
        :param letter: letter typed by the user
        :return: None
        """
        if not letter:
            self.error_type = 4
        elif letter == "exit":
            self.hangman.status = 1
        elif len(letter) > 1:
            self.error_type = 3
        elif self.hangman.letter_already_used(letter):
            self._handle_error(1)
        elif not self.hangman.letter_on_hide_word(letter):
            self._handle_error(2)
        else:
            self._handle_success(letter)

    def save_game(self, new=True):
        """
        Will save the current game into the storage managed by the model
        :param new: if is new will make a save if is not new will make an update
        :return: None
        """
        if not self.hangman:
            raise ExceptionGameNotInitialized("hangman")
        if new:
            self.hangman.save()
        else:
            self.hangman.update()

    def load_game(self, game_id):
        """
        Load a game previously saved by game_id
        :param game_id: game_id taht want be loaded
        :return:
        """
        self.hangman = Hangman.get_by_game_id(game_id)

    @property
    def num_errors(self):
        """
        Property that return the number of errors on the game
        :return: number of errors
        :rtype: int
        """
        return self.hangman.num_errors

    @property
    def end(self):
        """
        Property that return True if tis the end fo the game
        :return: True if the game is already finished, False otherwise
        """
        return self.hangman.status == 1

    @property
    def game_id(self):
        """
        Property that return the game_id of the Hangman instance
        :return: the game_id if there is game on going
        """
        return self.hangman.game_id

    @property
    def score(self):
        """
        Property that return the score of the Hangman instance
        :return: the game_id if there is game on going
        """
        return self.hangman.score

    @property
    def win(self):
        """
        Property that return tif the user won or not of the Hangman instance
        :return: True if the user won, False if not
        """
        return self.hangman.status == 1 and self.hangman.num_errors < MAX_FAILURES

    @staticmethod
    def get_ranking():
        """
        Get from teh Model the top of games leaving the logic of what is a top into the model

        :return: list of games
        """
        hangman = Hangman()
        return hangman.get_top(TOP_RANKING)

    def serializer(self):
        """
        Method that return the game serialized
        :return: dictionary with the game serialized
        """
        if not self.hangman:
            raise ExceptionGameNotInitialized("hangman")
        data_serialized = self.hangman.serializer()
        data_serialized["word"] = self.get_word_with_underscores()
        return data_serialized
