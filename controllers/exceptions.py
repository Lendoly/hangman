class ExceptionGameNotInitialized(BaseException):
    def __init__(self, game):
        self.game = game

    def __str__(self):
        return f"Gane {self.game} is not initialized"
