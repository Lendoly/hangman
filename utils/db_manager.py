import dataset

from constants import DATABASE_HOST


class DBManager:
    """
    DBManage handle the connect to the database
    """

    def __init__(self):
        self.db = dataset.connect(DATABASE_HOST)
