# HANGMAN GAME
Project that gives two easy ways to place to this popular game, by terminal and by API

## Hangman game rules
In case you didn't have childhood and played it between lessons, here is brief summary:

- This game is about to guess one word that the machine will propose to you.
- At the start you will only see underscores like this one '_', one underscore per letter
- The game will ask for one letter, if the letter that you introduce is inthe word the underscores will reveal letter

So for example '_ _ _ _ _' for the word 'o r d e r' if you introduce the letter r it will change to:
'\_ r _ _ r'

## What this project provide
In this project you will find only one main file

Depending which param you will execute it will run the terminal or the flask server

**IMPORTANT**: the game on the terminal should work with Linux and MacOx, but it's not tested on Windows

Additionally also provides a database (games.db) with extra data, if is not required just 
delete the file, on the startup another empty database will be created.

## Structure
The project is created following a MVC architecture:
 
- There are two Views (called interfaces) one for the terminal and another for the API
- There is only one controller
- There is only one model

Which give as result an architecture that looks like this: 


```mermaid
graph TD;
  api_user_interface.py --- controller.py;
  terminal_user_interface.py --- controller.py;
  controller.py --- hangman.py;
```

### Interfaces
#### api_user_interface

Contains all the possible endpoints which are:

| Route | supported methods | functionality | body |
|-------|-------------------|---------------|------|
| /hangman/game | POST | Create a new game| {"player_name": "luis"} |
| /hangman/game/<string:game_id> | GET | Retrieve info about a game||
| /hangman/game/<string:game_id>/letter | POST | Send a new letter | {"letter": "letter"} |
| /hangman/leaderboard | GET | Retrieve the leaderboard||

All of the support JSON and are in REST format

##### Create a new game

Will create a new game and save it into the database returning the info about the game

Request example with CURL and Localhost

```
curl -X POST  localhost:5000/hangman/game -d '{ "player_name": "luis" }'
```

expected response:

```json
{
  "game_info": {
    "game_id": "abfa4", 
    "guessed_letters": {}, 
    "num_errors": 0, 
    "player_name": "luis", 
    "score": 0, 
    "status": 0, 
    "word": "_ _ _ _ _", 
    "wrong_guessed_letters": []
  }
}
```

Possible errors: 

| HTTP CODE | Reason|
|-----------|-------| 
| 400 | player_name not in the body|

##### Retrieve info about a game

Will return the info about a game that will be specified on the url

Request example with CURL and Localhost for the game: ad064

```
curl -X GET  localhost:5000/hangman/game/ad064
```

expected response:

```json
{
    "game_info": {
        "game_id": "ad064",
        "guessed_letters": {},
        "num_errors": 0,
        "player_name": "AAA",
        "score": 0,
        "status": 0,
        "word": "_ _ _ _ _ _",
        "wrong_guessed_letters": []
    }
}
```
Possible errors: 

| HTTP CODE | Reason|
|-----------|-------| 
| 404 | game does not exists |

##### Send a new letter

Will accept a letter to evaluate into the game

Request example with CURL and Localhost and letter n

```
curl -X POST http://0.0.0.0:5000/hangman/game/98d8e/letter -d { "letter": "n"}'
```

Letter is part of the word:
```json
{
    "game_info": {
        "game_id": "b9933",
        "guessed_letters": {
            "d": 1
        },
        "num_errors": 1,
        "player_name": "luis",
        "score": 5,
        "status": 0,
        "word": "_ d _ _ _ _",
        "wrong_guessed_letters": [
            "r"
        ]
    }
}
```

Letter is not part of the word:
```json
{
    "game_info": {
        "game_id": "b9933",
        "guessed_letters": {},
        "num_errors": 1,
        "player_name": "luis",
        "score": -5,
        "status": 0,
        "word": "_ _ _ _ _ _",
        "wrong_guessed_letters": [
            "r"
        ]
    },
    "status": "UPS the letter is not on the word!"
}
```

Possible errors: 

| HTTP CODE | Reason|
|-----------|-------| 
| 404 | game does not exists |
| 400 | Letter is not on the body|



##### Retrieve the leaderboard

Will return the leader board of the whole game

Request example with CURL and Localhost

```
curl -X POST  http://0.0.0.0:5000/hangman/leaderboard
```

expected response:

```json
{
    "data": [
        {
            "game_info": {
                "game_id": "ea81d",
                "guessed_letters": {
                    "a": 1,
                    "i": 1,
                    "m": 1,
                    "n": 1,
                    "r": 1,
                    "v": 1
                },
                "num_errors": 0,
                "player_name": "AAA",
                "score": 60,
                "status": 1,
                "wrong_guessed_letters": []
            },
            "position": 1
        },
        ...
    ],
    "total_results": 10
```

#### api_user_interface

The user interface will ask first for the player name, after that will show a welcome message and give to the user 
three options:

0. Exit
1. Start a new game
2. Check leaderboard

it expect to introduce a number bewtween 0 and 2, if not the input is wrong will show an error and ask again

If the user take the option 2 (check leaderboard) it will be printed on the terminal and wait to any input form the user
to show again the first menu

If the user take the option 1 the game will start

### Controllers
#### Controller.py
This file is the controller for the hangman and handle all the business logic
#### Exceptions.py
This file contains exceptions that can be triggered by the controllers

### Models
#### Mixins
Folder wit the db_mixin which have the basic logic for connect to the database, so if we would like to create a new
game into this project that also needs DB connection it just need to inherit this class and will have the basics.

#### Hangman.py
Model of the hangman, it keeps all the logic of object modification and storage into the database

## How to use it

This guide will explain how to run it in two ways, Basic wich will run it locally (terminal and flask server)
and with Docker (just the flask server)
.
Basic:
1. (optional but HIGHLY recommend) create a virtual env
2. Install all dependencies with `pip install requirements.txt`
3. Run the project
    1. To run the terminal game just execute the command `python main.py terminal`
    2. To run the flask server just execute the command `python main.py flask`

There are two files that are also interesting to check
- constants.py: have the info of all the constants used in the project (including database URL)
- flask_config.py: have a basic configuration for flask
  
 Docker (only the flask server and not production ready):
 1. docker build -f Dockerfile-local -t hangman:latest .
 2. docker run -d -p 5000:5000 hangman:latest
 
## What else to improve?
Here is a brief list of things that could be improved or implemented:
- Be able to save and load games in the terminal
- ~~Dockerize the flask app~~
- Integration tests
- Setting in env variables
- Add more game to the catalog
