import uuid

from utils.db_manager import DBManager

db_manager = DBManager()


class DBMixin:
    """
    Mixin from which any future model can inherit and will give to it all the basic db functions
    """

    id = None
    game_id = None

    def __init__(self):
        self.id = None
        self.game_id = uuid.uuid4().hex[:5]
        self.table = db_manager.db.get_table(self.__class__.__name__.lower())

    def _to_dict(self):
        raise NotImplementedError

    def save(self):
        """
        Will save the instance into the database
        :return: None
        """
        self.table.insert(self._to_dict())

    def update(self):
        """
        Will update the current instance in the daatabase with teh current info
        :return: None
        """
        self.table.update(self._to_dict(), ["game_id"])
