import json
import random
from collections import Counter

from constants import POSSIBLE_WORDS
from models.mixins.db_mixin import DBMixin
from utils.db_manager import DBManager

CORRECT_POINTS = 10
INCORRECT_POINTS = 5

db_manager = DBManager()


class Hangman(DBMixin):
    """
    Model for the hangman game
    attributes:
        - Word: trying that contains the word that the user is trying to guess
        - num_errors: number of errors done by the player
        - status: 0 means still on going, 1 means game ended
        - guessed_letters: letters already guessed byt he player
        - wrong_guessed_letters: letters guessed wrong by the user
        - score: current core for the game
        - player_name: name of the player
    """

    word = None
    num_errors = 0
    status = 0
    guessed_letters = None
    wrong_guessed_letters = []
    score = 0
    player_name = None

    def __init__(self, **kwargs):
        super(Hangman, self).__init__()
        self.word = random.choice(POSSIBLE_WORDS)
        self.guessed_letters = Counter()
        self.wrong_guessed_letters = []
        self.status = 0
        self.score = 0
        self.player_name = kwargs["player_name"] if "player_name" in kwargs else ""
        for key in kwargs:
            if key == "guessed_letters":
                setattr(self, key, Counter(json.loads(kwargs[key])))
            elif key == "wrong_guessed_letters":
                if kwargs[key]:
                    setattr(self, key, kwargs[key].split(","))
            else:
                setattr(self, key, kwargs[key])
        self.word_counter = Counter(self.word)

    def letter_already_used(self, letter):
        """
        Function that check if one letter is already used or not
        :param letter: letter for check
        :type letter: str
        :return: True if was already used, False otherwise
        :rtype: bool
        """
        if letter in self.guessed_letters or letter in self.wrong_guessed_letters:
            return True
        return False

    def letter_on_hide_word(self, letter):
        """
        Checks if the letter is on the word, if it is in the word also add the letter
        to the guessed_letters attribute if is not there add the letter to the worng_guessed_letters attr
        :param letter: letter to check
        :type letter: str
        :return: True if is there false otherwise
        :rtype: bool
        """
        if letter in self.word:
            self.guessed_letters[letter] = self.word_counter.get(letter)
            return True
        else:
            self.wrong_guessed_letters.append(letter)
            return False

    def increase_score(self, letter):
        """
        Increase the score base on the rules, which are will add to the current score
        the number of points for guess correct multiply for every time that the letter appears
        :param letter: letter to check how many times it appear
        :return: None
        """
        if letter in self.word_counter:
            self.score += CORRECT_POINTS * self.word_counter.get(letter)

    def add_error(self):
        """
        Deduct points fom the current score base on the rules and add a new failure to the count
        :return:
        """
        self.num_errors += 1
        self.score -= INCORRECT_POINTS

    def _to_dict(self, for_api=False):
        """
        Internal method to transform the object to a dictionary with the correct format to be serialized for the database
        of the api
        :param for_api: indicate if it need to be transformed in a dictionary that can be use on an REST API or just
        a normal dict
        :return: dict
        """
        instance_dict = {
            "game_id": self.game_id,
            "num_errors": self.num_errors,
            "status": self.status,
            "score": self.score,
            "player_name": self.player_name,
        }
        if for_api:
            instance_dict["guessed_letters"] = dict(self.guessed_letters)
            instance_dict["wrong_guessed_letters"] = self.wrong_guessed_letters
        else:
            instance_dict["word"] = self.word
            instance_dict["guessed_letters"] = json.dumps(dict(self.guessed_letters))
            instance_dict["wrong_guessed_letters"] = ",".join(
                self.wrong_guessed_letters
            )
        return instance_dict

    @property
    def correct(self):
        """
        Property that check if the actual amount of guessed letter match with the whole word

        :return: If they match yes will return True, False if not.
        :rtype: bool
        """
        return self.guessed_letters == self.word_counter

    def get_top(self, number_of_elements):
        """
        Method that query the database and return a list of games, order by the score in a descendant order
        :param number_of_elements: number of elements to query
        :return: list of hangman instances
        """
        return_list = []
        result = self.table.find(_limit=number_of_elements, order_by="-score")
        for element in result:
            return_list.append(Hangman(**element))

        return return_list

    def serializer(self):
        """
        Method that return the instance on a serializable dictionary
        :return:
        """
        return self._to_dict(for_api=True)

    @staticmethod
    def get_by_game_id(game_id):
        """
        Given a game_id will search in teh database for it and return a Hangman instance
        :param game_id: game_id to search into the database
        :return: hangman instance or None
        :rtype: Hangman
        """
        table = db_manager.db.get_table("hangman")
        result = table.find_one(game_id=game_id)
        if not result:
            return None
        return Hangman(**result)

    def __str__(self):
        return f"{self.player_name}\t{self.score}"

    def __repr__(self):
        return self.__str__()
